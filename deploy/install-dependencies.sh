#!/bin/bash

# remove obsolete packages if installed
#sudo python -m pip uninstall enum

echo "Install Python dependencies"
#sudo apt-get install -y python-pip3
sudo python3 -m pip install configparser paho-mqtt pyudev ioctl_opt simplejson
sudo apt-get -y install --reinstall python-enum34
